package de.fuchspfoten.endzeit;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A task that can be executed.
 */
public class Task {

    /**
     * Loads a task from the given {@link org.bukkit.configuration.ConfigurationSection}.
     *
     * @param section The section from which the task is loaded.
     * @return The loaded task.
     */
    public static Task load(final ConfigurationSection section) {
        return new Task(section.getLong("schedule"), section.getLong("period"),
                section.getStringList("commands"), section.getLong("skipDelta"));
    }

    /**
     * The period for repeating tasks. 0 for non-repeating tasks.
     */
    private final long period;

    /**
     * The commands this task contains.
     */
    private final Collection<String> commands = new ArrayList<>();

    /**
     * The maximum time delta before an execution is skipped. {@code 0} to disable.
     */
    private final long skipDelta;

    /**
     * The next scheduled command execution.
     */
    private @Getter long nextScheduledExecution;

    /**
     * Constructor.
     *
     * @param nextScheduledExecution The next scheduled execution.
     * @param period                 The period with which this task is executed.
     * @param commands               The commands for this task.
     * @param skipDelta              The skip delta for this task.
     */
    public Task(final long nextScheduledExecution, final long period, final Collection<String> commands,
                final long skipDelta) {
        this.nextScheduledExecution = nextScheduledExecution;
        this.period = period;
        this.commands.addAll(commands);
        this.skipDelta = skipDelta;
    }

    /**
     * Executes this task.
     */
    public void execute() {
        if (!isScheduled() || nextScheduledExecution == -1) {
            return;
        }

        final long timeDelta = System.currentTimeMillis() - nextScheduledExecution;
        if (isRepeating()) {
            nextScheduledExecution += period;
        }

        if (skipDelta > 0 && timeDelta >= skipDelta) {
            // Task should not be executed after so long.
            return;
        }
        commands.forEach(x -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), x));
    }

    /**
     * Saves this task to the given configuration section.
     *
     * @param section The section.
     */
    public void save(final ConfigurationSection section) {
        section.set("schedule", nextScheduledExecution);
        section.set("period", period);
        section.set("commands", commands);
        section.set("skipDelta", skipDelta);
    }

    /**
     * Checks whether this task is scheduled.
     *
     * @return {@code true} iff this task is scheduled.
     */
    public boolean isScheduled() {
        return nextScheduledExecution != 0;
    }

    /**
     * Checks whether this task is repeating (period != 0).
     *
     * @return {@code true} iff this task is repeating.
     */
    private boolean isRepeating() {
        return period != 0;
    }
}

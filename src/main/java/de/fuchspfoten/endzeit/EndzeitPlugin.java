/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.endzeit;

import de.fuchspfoten.fuchslib.util.Pair;
import lombok.Getter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Plugin for being loaded by Bukkit.
 */
public class EndzeitPlugin extends JavaPlugin {

    /**
     * Singleton plugin instance.
     */
    private @Getter static EndzeitPlugin self;

    /**
     * The task execution queue.
     */
    private final Queue<Pair<String, Task>> taskExecutionQueue = new LinkedList<>();

    /**
     * Checks tasks and executes them if needed.
     */
    private void checkTasks() {
        if (taskExecutionQueue.isEmpty()) {
            return;
        }

        final Pair<String, Task> nextTask = taskExecutionQueue.poll();
        if (System.currentTimeMillis() >= nextTask.getSecond().getNextScheduledExecution()) {
            nextTask.getSecond().execute();
            getLogger().info("Executed task " + nextTask.getFirst());

            // Update task information or remove the task after it is executed.
            if (nextTask.getSecond().isScheduled()) {
                nextTask.getSecond().save(getConfig().getConfigurationSection("tasks." + nextTask.getFirst()));
                taskExecutionQueue.add(nextTask);
            } else {
                getConfig().getConfigurationSection("tasks").set(nextTask.getFirst(), null);
            }
        } else {
            taskExecutionQueue.add(nextTask);
        }
    }

    @Override
    public void onDisable() {
        saveConfig();
    }

    @Override
    public void onEnable() {
        self = this;

        // Create default config.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Load tasks.
        final ConfigurationSection taskSection = getConfig().getConfigurationSection("tasks");
        for (final String key : taskSection.getKeys(false)) {
            taskExecutionQueue.add(new Pair<>(key, Task.load(taskSection.getConfigurationSection(key))));
        }

        // Start the runner.
        getServer().getScheduler().scheduleSyncRepeatingTask(this, this::checkTasks, 30 * 20L, 5L);

        getLogger().info("Started timer at " + System.currentTimeMillis() + " with " + taskExecutionQueue.size()
                + " tasks");
    }
}
